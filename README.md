# GND-Nummern von Personen im halleschen Druckwesen (16. – 18. Jh.)

Die Auseinandersetzung mit der Geschichte des Buchdrucks in Halle an der Saale reicht zurück bis in das 18. Jahrhundert. Und auch wenn Christian Friedrich Gessner [im dritten Band](http://www.deutschestextarchiv.de/gessner_buchdruckerkunst03_1741/347) seines Werks über _Die so nöthig als nützliche Buchdruckerkunst und Schriftgießerei_ (1741) im Unklaren darüber ist, wer in dieser Stadt den Anfang gemacht hat – „Wer allhier der erſte Buchdrucker geweſen iſt, laͤßt ſich aus Mangel der Nachrichten ſo genau nicht beſtimmen.“ –, so weiß er doch insgesamt 36 Personen aufzuzählen, die in der Zeit von 1597 bis in seine Gegenwart nachweislich in Halle tätig waren.

Solche Verzeichnisse, wie schon Gessner sie in den Bänden der _Buchdruckerkunst_ für verschiedene deutsche Städte präsentiert, sind über die Jahrhunderte kontinuierlich aufgegriffen, überarbeitet und ergänzt worden. In der jüngeren Zeit sind es nicht zuletzt die Nachschlagewerke von Joseph Benzing (1963/1982), David L. Paisey (1988) und Christoph Reske (2007/2015), die das Wissen um die deutschen Drucker und Verleger des 16., 17. und frühen 18. Jahrhunderts zugänglich machen. Diesen Werken ist es auch zu verdanken, dass die in ihnen beschriebenen Personen allesamt in der Gemeinsamen Normdatei (GND) erfasst sind, wobei neben etwaigen Namensvarianten hier auch Tätigkeitszeiträume und Wirkungsorte übernommen wurden.

In diesem Repositorium sind nun [JSON-Dateien](./data/) zu finden, welche GND-Nummern von Personen beinhalten, die anhand der folgend genannten Quellen als „tätig in Halle“ recherchiert wurden.

## Quellen

### Benzing, Josef

- _Die Buchdrucker des 16. und 17. Jahrhunderts im deutschen Sprachgebiet_
    - Wiesbaden: Harrassowitz, 1963 ([DNB](http://d-nb.info/450361772))
    - 2., verbesserte und ergänzte Auflage. Wiesbaden: Harrassowitz 1982 ([DNB](http://d-nb.info/1045415405))
        - Fachliches Nachschlagewerk für die Gemeinsame Normdatei, 2019 ([URN](https://nbn-resolving.org/urn:nbn:de:101-2019032700))
- _Die deutschen Verleger des 16. und 17. Jahrhunderts. Eine Neubearbeitung_
    - Archiv für Geschichte des Buchwesens 28 (1977), Sp. 1077–1322


### Reske, Christoph

- _Die Buchdrucker des 16. und 17. Jahrhunderts im deutschen Sprachgebiet. Auf der Grundlage des gleichnamigen Werkes von Josef Benzing_.
    - Wiesbaden: Harrassowitz, 2007 ([DNB](http://d-nb.info/983950849))
        - Fachliches Nachschlagewerk für die Gemeinsame Normdatei, 2019 ([URN](https://nbn-resolving.org/urn:nbn:de:101-2019032700))
    - 2., überarbeitete und erweiterte Auflage. Wiesbaden: Harrassowitz, 2015 ([DNB](http://d-nb.info/1070778834))


### Paisey, David L.

- _Deutsche Buchdrucker, Buchhändler und Verleger: 1701–1750_
    - Wiesbaden: Harrassowitz, 1988 ([DNB](http://d-nb.info/881177024))
- _German Printers, Booksellers and Publishers of the Seventeenth Century. Some Amendments and Additions to Benzing_
    - Gutenberg-Jahrbuch (1989), S. 165–179
